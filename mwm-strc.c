struct Button
{const Arg arg;
 void (*func)(const Arg *arg);
 unsigned int click,mask,button;
};

struct Client
{       Window win;
        Bool isfixed,isfloating,isurgent,neverfocus,oldstate,isfullscreen;
 struct Client *N;
 struct Client *snext;
 struct Monitor*Mon;
 float mina, maxa;
          int v,w,x,y,
              oldv,oldw,oldx,oldy,
              basev,basew,incv,incw,maxv,maxw,minv,minw,
              bw,oldbw;
 unsigned int tags;
 char name[256];
};

struct DC
{struct
 {XFontSet    set;
  XFontStruct*xfont;
          int ascent,descent,height;
 }font;
 Drawable drawable;
       GC gc;
 int v,w,x,y;
 unsigned long norm[ColLast],
                sel[ColLast];
}; /* draw context */

struct Key
{unsigned int mod;
 KeySym keysym;
 void (*func)(const Arg *);
 const Arg arg;
};

struct Layout
{const char *symbol;
 void (*arrange)(struct Monitor *);
};

struct Monitor
{char ltsymbol[16];
 float mfact;
 int nmaster;
 int num;
 int by;               /* bar geometry */
 int mv, mw, mx, my;   /* screen size */
 int wv, ww, wx, wy;   /* window area  */
 unsigned int seltags;
 unsigned int sellt;
 unsigned int tagset[2];
 Bool showbar;
 Bool topbar;
 struct Client *Clients;
 struct Client *Sel;
 struct Client *Stack;
 struct Monitor*N;
        Window barwin;
 const struct Layout*Lt[2];
};

struct Rule
{const char*class;
 const char*instance;
 const char*title;
 Bool isfloating;
 unsigned int tags;
 int monitor;
};

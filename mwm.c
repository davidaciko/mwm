/******************************************************************************\
|****                             mwm v0.0.2                               ****|
|**   mwm (Meli Window Manager) is based off of dwm (Dynamic Window Manager) **|
|**  version 6.0 (licensed under the MIT license) and dvtm (Dynamic Virtual  **|
|**  Tiling Manager) version 0.9 (also licensed under the MIT license), and  **|
|**  is intended to unify both under a single executable file (mwm).         **|
|**                                                                          **|
|**   mwm is written in pure C89 (ANSI X3.159-1989, ISO/IEC 9899:1990), and  **|
|**  is intended to be compiled by the GCC (GNU Compiler Collection), but it **|
|**  should compile just fine with virtually any C compiler.                 **|
|**                                                                          **|
|**   Although mwm is based off of dwm and dvtm, it is not intended to       **|
|**  mimic the behaviour of those projects.  The primary reason I decided to **|
|**  write mwm is to tweak the behaviour of dwm and dvtm to suit my personal **|
|**  working style, but I am releasing it publically with the hope that it   **|
|**  can be of use to other people as well.                                  **|
|**                                                                          **|
|**  Planned Features:                                                       **|
|**   [ ]  Small memory footprint, but execution speed is the top priority.  **|
|**   [ ]  Better default layouts based on use (edit/reference areas).       **|
|**   [ ]  Edit-master, Reference-master, and Reference-secondary areas.     **|
|**   [ ]  Easier configuration still done entirely in a single header file. **|
|**   [ ]  Default configuration files for US-QWERTY and US-Dvorak layouts.  **|
|**   [ ]  Mode-ful capable, like in vi/vim, and current mode in status bar. **|
|**   [ ]  Ability to run more pre-defined commands via hotkeys.             **|
|**   [ ]  Area sizes in clean fractions of 24, not ugly percents.           **|
|**   [ ]  Various functions offered as patches and plugins to dwm.          **|
|**   [ ]  Customisable text-selection mode for textmode (dvtm's Copy Mode). **|
|**   [ ]  Makefile packages source upon binary build and command to unpack. **|
|**   [ ]  Original source code package also copied, and command to unpack.  **|
|**   [ ]  Command to download fresh versions of source code.                **|
|**   [x]  Get rid of all the lazy typedefs!                                 **|
|**                                                                          **|
|**  Features to be considered later:                                        **|
|**   [ ]  Keyboard-layout-agnostic configuration for gfx mode (scancodes).  **|
|**                                                                          **|
|**  Features that will not be included (so please don't ask for them):      **|
|**     -  Limited number of source lines of code.                           **|
|**     -  Tagging (mwm will divert from d*m and use workspaces instead)     **|
|**     -  Resoure-heavy features, such as scripting language integration.   **|
|****                                                                      ****|
\******************************************************************************/

/* See LICENSE file for copyright and license details.
 *
 * meli window manager is designed like any other X client as well. It is
 * driven through handling X events. In contrast to other X clients, a window
 * manager selects for SubstructureRedirectMask on the root window, to receive
 * events about window (dis-)appearance.  Only one X connection at a time is
 * allowed to select for this event mask.
 *
 * The event handlers of mwm are organized in an array which is accessed
 * whenever a new event has been fetched. This allows event dispatching
 * in O(1) time.
 *
 * Each child of the root window is called a client, except windows which have
 * set the override_redirect flag.  Clients are organized in a linked client
 * list on each monitor, the focus history is remembered through a stack list
 * on each monitor. Each client contains a bit array to indicate the tags of a
 * client.
 *
 * Keys and tagging rules are organized as arrays and defined in config.h.
 *
 * To understand everything else, start reading main().
 */

#include "mwm-incl.c"
#include "mwm-strc.c"
#include "mwm-fdec.c"
#include "mwm-vars.c"

/* configuration, allows nested code to access above variables */
#include "config.h"

/* compile-time check if all tags fit into an unsigned int bit array. */
struct NumTags { char limitexceeded[LENGTH(tags) > 31 ? -1 : 1]; };

#include "mwm-fimp.c"

void
run(void)
{XEvent ev;
 /* main event loop */
 XSync(Dpy,False);
 while(running&&!XNextEvent(Dpy,&ev))
  switch(ev.type)
  {case ButtonPress:
    buttonpress(&ev);
    break;
   case ClientMessage:
    clientmessage(&ev);
    break;
   case ConfigureRequest:
    configurerequest(&ev);
    break;
   case ConfigureNotify:
    configurenotify(&ev);
    break;
   case DestroyNotify:
    destroynotify(&ev);
    break;
   case EnterNotify:
    enternotify(&ev);
    break;
   case Expose:
    expose(&ev);
    break;
   case FocusIn:
    focusin(&ev);
    break;
   case KeyPress:
    keypress(&ev);
    break;
   case MappingNotify:
    mappingnotify(&ev);
    break;
   case MapRequest:
    maprequest(&ev);
    break;
   case MotionNotify:
    motionnotify(&ev);
    break;
   case PropertyNotify:
    propertynotify(&ev);
    break;
   case UnmapNotify:
    unmapnotify(&ev);
    break;
   default:
    break;
  };
 /*
 while(running&&!XNextEvent(Dpy,&ev))
  if(handler[ev.type])
   handler[ev.type](&ev); /+ call handler +/
 */
}

int
main(int z,char**Z)
{if(z==2&&!strcmp("-v",Z[1]))
 die("mwm-"VERSION", © 2014-2014 davidaciko, see LICENSE for details\n");
 else if(z!=1)
  die("mwm: usage: mwm [-v]\n");
 if(!setlocale(LC_CTYPE,"")||!XSupportsLocale())
  fputs("mwm: warning: no locale support\n",stderr);
 if(!(Dpy=XOpenDisplay(NULL)))
  die("mwm: error: cannot open display\n");
 checkotherwm();
 setup();
 scan();
 run();
 cleanup();
 XCloseDisplay(Dpy);
 return EXIT_SUCCESS;
}

# mwm version
VERSION = 0.0.2

# Customize below to fit your system

# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

# Xinerama
XINERAMALIBS = -L${X11LIB} -lXinerama
XINERAMAFLAGS = -DXINERAMA

# includes and libs
INCS = -I. -I/usr/include -I${X11INC}
LIBS = -L/usr/lib -lc -L${X11LIB} -lX11 ${XINERAMALIBS}

# flags
CPPFLAGS = -DVERSION=\"${VERSION}\" ${XINERAMAFLAGS}
# Reference, always commented out:
#CFLAGS = -g -std=c89 -pedantic -Wall -O0 ${INCS} ${CPPFLAGS}
# Release, keep only this:
#CFLAGS = -std=c89 -pedantic -Wall -Wextra -Wno-unused-function -Wno-unused-value -O3 ${INCS} ${CPPFLAGS}
# Debug to show only errors:
CFLAGS = -std=c89 -pedantic -Wno-unused-function -Wno-unused-value -O3 ${INCS} ${CPPFLAGS}
#LDFLAGS = -g ${LIBS}
LDFLAGS = -s ${LIBS}

# Solaris
#CFLAGS = -fast ${INCS} -DVERSION=\"${VERSION}\"
#LDFLAGS = ${LIBS}

# compiler and linker
CC = cc

/* function implementations */
void
applyrules(struct Client*c)
{const struct Rule   *r       ;
 const        char   *class   ,
                     *instance;
 XClassHint ch={NULL,NULL}    ;
       struct Monitor*m       ;
 unsigned     int     i       ;

 /* rule matching */
 c->isfloating=c->tags=0;
 XGetClassHint(Dpy,c->win,&ch);
 class   =ch.res_class
         ?ch.res_class
         :broken;
 instance=ch.res_name
         ?ch.res_name
         :broken;
 for(i=0;i<LENGTH(rules);i++)
 {r=&rules[i];
  if((!r->title   ||strstr(c->name    ,r->title   ))
   &&(!r->class   ||strstr(   class   ,r->class   ))
   &&(!r->instance||strstr(   instance,r->instance)))
  {c->isfloating =r->isfloating;
   c->tags      |=r->tags      ;
   for(m=Mons;m&&m->num!=r->monitor;m=m->N);
   if(m)
    c->Mon=m;
  }
 }
 if(ch.res_class)
  XFree(ch.res_class);
 if(ch.res_name)
  XFree(ch.res_name);
 c->tags=c->tags&TAGMASK
        ?c->tags&TAGMASK
        :c->Mon->tagset[c->Mon->seltags];
}

Bool
applysizehints(struct Client*c,int*x,int*y,int*w,int*v,Bool interact)
{struct Monitor*m        =c->Mon;
/*        Bool    baseismin       ;*/
 /* set minimum possible */
 *v=MAX(1,*v);
 *w=MAX(1,*w);
 if(interact)
 {if(*y>       sv        )*y=      sv   -VERTH(c);
  if(*x>       sw        )*x=      sw   -WIDTH(c);
  if(*y+*v+2*c->bw< 0    )*y=0                   ;
  if(*x+*w+2*c->bw< 0    )*x=0                   ;
 }else
 {if(*y>=m->wy+m->wv     )*y=m->wy+m->wv-VERTH(c);
  if(*x>=m->wx+m->ww     )*x=m->wx+m->ww-WIDTH(c);
  if(*y+*v+2*c->bw<=m->wy)*y=m->wy               ;
  if(*x+*w+2*c->bw<=m->wx)*x=m->wx               ;
 }
 if(    *v        <bh    )*v=bh                  ;
 if(    *w        <bh    )*w=bh                  ;
 if(resizehints||c->isfloating||!c->Mon->Lt[c->Mon->sellt]->arrange)
 /* see last two sentences in ICCCM 4.1.2.3 */
 {/*baseismin=c->basew==c->minw&&c->basev==c->minv;*/
  if(c->basev==c->minv
   &&c->basew==c->minw)
  {/* adjust for aspect limits */
   if(c->mina>0
    &&c->maxa>0)
   {     if(c->maxa<(float)*w/(*v))
     *w=*v*c->maxa+0.5; /*,//// 0.5? */
    else if(c->mina<(float)*v/(*w))
     *v=*w*c->mina+0.5; /*,//// 0.5? */
   }
  /* if( baseismin) */
  /* increment calculation requires this */
   *v-=c->basev;
   *w-=c->basew;
  }else
  /* temporarily remove base dimensions */
  {*v-=c->basev;
   *w-=c->basew;
   /* adjust for aspect limits */
   if(c->mina>0
    &&c->maxa>0)
   {     if(c->maxa<(float)*w/(*v))
     *w=*v*c->maxa+0.5; /*,//// 0.5? */
    else if(c->mina<(float)*v/(*w))
     *v=*w*c->mina+0.5; /*,//// 0.5? */
   }
  }
  /* adjust for increment value *//*,//// "increment value"=snap-to-modulus value*/
  if(c->incv)*v-=*v%c->incv;
  if(c->incw)*w-=*w%c->incw;
  /* restore base dimensions */
  *v=MAX(*v+c->basev,c->minv);
  *w=MAX(*w+c->basew,c->minw);
  if(c->maxv)*v=MIN(*v,c->maxv);
  if(c->maxw)*w=MIN(*w,c->maxw);
 }
 return *x!=c->x||*y!=c->y||*v!=c->v||*w!=c->w;
}

void
arrange(struct Monitor*m)
{if(m)
  showhide(m->Stack);
 else
  for(m=Mons;m;m=m->N) /*,//// TODO: s/;m;/;;/ possible?? */
   showhide(m->Stack);
 if(m)
  arrangemon(m);
 else
  for(m=Mons;m;m=m->N) /*,//// TODO: s/;m;/;;/ possible?? */
   arrangemon(m);
}

void
arrangemon(struct Monitor*m)
{strncpy(m->ltsymbol,m->Lt[m->sellt]->symbol,sizeof m->ltsymbol); /*,//// TODO: change sizeof to sizeof() */
 if(m->Lt[m->sellt]->arrange)
  m->Lt[m->sellt]->arrange(m);
 restack(m);
}

void
attach(struct Client*c) {
	c->N = c->Mon->Clients;
	c->Mon->Clients = c;
}

void
attachstack(struct Client*c) {
	c->snext = c->Mon->Stack;
	c->Mon->Stack = c;
}

void
buttonpress(XEvent *e) {
	unsigned int i, x, click;
	Arg arg = {0};
	struct Client*c;
	struct Monitor *m;
	XButtonPressedEvent *ev = &e->xbutton;

	click = ClkRootWin;
	/* focus monitor if necessary */
	if((m = wintomon(ev->window)) && m != Selmon) {
		unfocus(Selmon->Sel, True);
		Selmon = m;
		focus(NULL);
	}
	if(ev->window == Selmon->barwin) {
		i = x = 0;
		do
			x += TEXTW(tags[i]);
		while(ev->x >= x && ++i < LENGTH(tags));
		if(i < LENGTH(tags)) {
			click = ClkTagBar;
			arg.ui = 1 << i;
		}
		else if(ev->x < x + blw)
			click = ClkLtSymbol;
		else if(ev->x > Selmon->ww - TEXTW(stext))
			click = ClkStatusText;
		else
			click = ClkWinTitle;
	}
	else if((c = wintoclient(ev->window))) {
		focus(c);
		click = ClkClientWin;
	}
	for(i = 0; i < LENGTH(buttons); i++)
		if(click == buttons[i].click && buttons[i].func && buttons[i].button == ev->button
		&& CLEANMASK(buttons[i].mask) == CLEANMASK(ev->state))
			buttons[i].func(click == ClkTagBar && buttons[i].arg.i == 0 ? &arg : &buttons[i].arg);
}

void
checkotherwm(void) {
	xerrorxlib = XSetErrorHandler(xerrorstart);
	/* this causes an error if some other window manager is running */
	XSelectInput(Dpy, DefaultRootWindow(Dpy), SubstructureRedirectMask);
	XSync(Dpy, False);
	XSetErrorHandler(xerror);
	XSync(Dpy, False);
}

void
cleanup(void) {
	Arg a = {.ui = ~0};
	struct Layout foo = { "", NULL };
	struct Monitor *m;

	view(&a);
	Selmon->Lt[Selmon->sellt] = &foo;
	for(m = Mons; m; m = m->N)
		while(m->Stack)
			unmanage(m->Stack, False);
	if(dc.font.set)
		XFreeFontSet(Dpy, dc.font.set);
	else
		XFreeFont(Dpy, dc.font.xfont);
	XUngrabKey(Dpy, AnyKey, AnyModifier, root);
	XFreePixmap(Dpy, dc.drawable);
	XFreeGC(Dpy, dc.gc);
	XFreeCursor(Dpy, cursor[CurNormal]);
	XFreeCursor(Dpy, cursor[CurResize]);
	XFreeCursor(Dpy, cursor[CurMove]);
	while(Mons)
		cleanupmon(Mons);
	XSync(Dpy, False);
	XSetInputFocus(Dpy, PointerRoot, RevertToPointerRoot, CurrentTime);
}

void
cleanupmon(struct Monitor*Mon) {
	struct Monitor *m;

	if(Mon == Mons)
		Mons = Mons->N;
	else {
		for(m = Mons; m && m->N != Mon; m = m->N);
		m->N = Mon->N;
	}
	XUnmapWindow(Dpy, Mon->barwin);
	XDestroyWindow(Dpy, Mon->barwin);
	free(Mon);
}

void
clearurgent(struct Client*c) {
	XWMHints *wmh;

	c->isurgent = False;
	if(!(wmh = XGetWMHints(Dpy, c->win)))
		return;
	wmh->flags &= ~XUrgencyHint;
	XSetWMHints(Dpy, c->win, wmh);
	XFree(wmh);
}

void
clientmessage(XEvent *e) {
	XClientMessageEvent *cme = &e->xclient;
	struct Client*c = wintoclient(cme->window);

	if(!c)
		return;
	if(cme->message_type == netatom[NetWMState]) {
		if(cme->data.l[1] == netatom[NetWMFullscreen] || cme->data.l[2] == netatom[NetWMFullscreen])
			setfullscreen(c, (cme->data.l[0] == 1 /* _NET_WM_STATE_ADD    */
			              || (cme->data.l[0] == 2 /* _NET_WM_STATE_TOGGLE */ && !c->isfullscreen)));
	}
	else if(cme->message_type == netatom[NetActiveWindow]) {
		if(!ISVISIBLE(c)) {
			c->Mon->seltags ^= 1;
			c->Mon->tagset[c->Mon->seltags] = c->tags;
		}
		pop(c);
	}
}

void
configure(struct Client*c) {
	XConfigureEvent ce;

	ce.type = ConfigureNotify;
	ce.display = Dpy;
	ce.event = c->win;
	ce.window = c->win;
	ce.x = c->x;
	ce.y = c->y;
	ce.height = c->v;
	ce.width = c->w;
	ce.border_width = c->bw;
	ce.above = None;
	ce.override_redirect = False;
	XSendEvent(Dpy, c->win, False, StructureNotifyMask, (XEvent *)&ce);
}

void
configurenotify(XEvent *e) {
	struct Monitor *m;
	XConfigureEvent *ev = &e->xconfigure;
	Bool dirty;

	if(ev->window == root) {
		dirty = (sw != ev->width);
		sw = ev->width;
		sv = ev->height;
		if(updategeom() || dirty) {
			if(dc.drawable != 0)
				XFreePixmap(Dpy, dc.drawable);
			dc.drawable = XCreatePixmap(Dpy, root, sw, bh, DefaultDepth(Dpy, screen));
			updatebars();
			for(m = Mons; m; m = m->N)
				XMoveResizeWindow(Dpy, m->barwin, m->wx, m->by, m->ww, bh);
			focus(NULL);
			arrange(NULL);
		}
	}
}

void
configurerequest(XEvent *e) {
	struct Client*c;
	struct Monitor *m;
	XConfigureRequestEvent *ev = &e->xconfigurerequest;
	XWindowChanges wc;

	if((c = wintoclient(ev->window))) {
		if(ev->value_mask & CWBorderWidth)
			c->bw = ev->border_width;
		else if(c->isfloating || !Selmon->Lt[Selmon->sellt]->arrange) {
			m = c->Mon;
			if(ev->value_mask & CWX) {
				c->oldx = c->x;
				c->x = m->mx + ev->x;
			}
			if(ev->value_mask & CWY) {
				c->oldy = c->y;
				c->y = m->my + ev->y;
			}
			if(ev->value_mask & CWWidth) {
				c->oldw = c->w;
				c->w = ev->width;
			}
			if(ev->value_mask & CWHeight) {
				c->oldv = c->v;
				c->v = ev->height;
			}
			if((c->x + c->w) > m->mx + m->mw && c->isfloating)
				c->x = m->mx + (m->mw / 2 - WIDTH(c) / 2); /* center in x direction */
			if((c->y + c->v) > m->my + m->mv && c->isfloating)
				c->y = m->my + (m->mv / 2 - VERTH(c) / 2); /* center in y direction */
			if((ev->value_mask & (CWX|CWY)) && !(ev->value_mask & (CWWidth|CWHeight)))
				configure(c);
			if(ISVISIBLE(c))
				XMoveResizeWindow(Dpy, c->win, c->x, c->y, c->w, c->v);
		}
		else
			configure(c);
	}
	else {
		wc.x = ev->x;
		wc.y = ev->y;
		wc.width = ev->width;
		wc.height = ev->height;
		wc.border_width = ev->border_width;
		wc.sibling = ev->above;
		wc.stack_mode = ev->detail;
		XConfigureWindow(Dpy, ev->window, ev->value_mask, &wc);
	}
	XSync(Dpy, False);
}

struct Monitor *
createmon(void) {
	struct Monitor *m;

	if(!(m = (struct Monitor *)calloc(1, sizeof(struct Monitor))))
		die("fatal: could not malloc() %u bytes\n", sizeof(struct Monitor));
	m->tagset[0] = m->tagset[1] = 1;
	m->mfact = mfact;
	m->nmaster = nmaster;
	m->showbar = showbar;
	m->topbar = topbar;
	m->Lt[0] = &layouts[0];
	m->Lt[1] = &layouts[1 % LENGTH(layouts)];
	strncpy(m->ltsymbol, layouts[0].symbol, sizeof m->ltsymbol);
	return m;
}

void
destroynotify(XEvent *e) {
	struct Client*c;
	XDestroyWindowEvent *ev = &e->xdestroywindow;

	if((c = wintoclient(ev->window)))
		unmanage(c, True);
}

void
detach(struct Client*c) {
	struct Client**tc;

	for(tc = &c->Mon->Clients; *tc && *tc != c; tc = &(*tc)->N);
	*tc = c->N;
}

void
detachstack(struct Client*c) {
	struct Client**tc,*t;

	for(tc = &c->Mon->Stack; *tc && *tc != c; tc = &(*tc)->snext);
	*tc = c->snext;

	if(c == c->Mon->Sel) {
		for(t = c->Mon->Stack; t && !ISVISIBLE(t); t = t->snext);
		c->Mon->Sel = t;
	}
}

void
die(const char *errstr, ...) {
	va_list ap;

	va_start(ap, errstr);
	vfprintf(stderr, errstr, ap);
	va_end(ap);
	exit(EXIT_FAILURE);
}

struct Monitor *
dirtomon(int dir) {
	struct Monitor *m = NULL;

	if(dir > 0) {
		if(!(m = Selmon->N))
			m = Mons;
	}
	else if(Selmon == Mons)
		for(m = Mons; m->N; m = m->N);
	else
		for(m = Mons; m->N != Selmon; m = m->N);
	return m;
}

void
drawbar(struct Monitor *m) {
	int x;
	unsigned int i, occ = 0, urg = 0;
	unsigned long *col;
	struct Client*c;

	for(c = m->Clients; c; c = c->N) {
		occ |= c->tags;
		if(c->isurgent)
			urg |= c->tags;
	}
	dc.x = 0;
	for(i = 0; i < LENGTH(tags); i++) {
		dc.w = TEXTW(tags[i]);
		col = m->tagset[m->seltags] & 1 << i ? dc.sel : dc.norm;
		drawtext(tags[i], col, urg & 1 << i);
		drawsquare(m == Selmon && Selmon->Sel && Selmon->Sel->tags & 1 << i,
		           occ & 1 << i, urg & 1 << i, col);
		dc.x += dc.w;
	}
	dc.w = blw = TEXTW(m->ltsymbol);
	drawtext(m->ltsymbol, dc.norm, False);
	dc.x += dc.w;
	x = dc.x;
	if(m == Selmon) { /* status is only drawn on selected monitor */
		dc.w = TEXTW(stext);
		dc.x = m->ww - dc.w;
		if(dc.x < x) {
			dc.x = x;
			dc.w = m->ww - x;
		}
		drawtext(stext, dc.norm, False);
	}
	else
		dc.x = m->ww;
	if((dc.w = dc.x - x) > bh) {
		dc.x = x;
		if(m->Sel) {
			col = m == Selmon ? dc.sel : dc.norm;
			drawtext(m->Sel->name, col, False);
			drawsquare(m->Sel->isfixed, m->Sel->isfloating, False, col);
		}
		else
			drawtext(NULL, dc.norm, False);
	}
	XCopyArea(Dpy, dc.drawable, m->barwin, dc.gc, 0, 0, m->ww, bh, 0, 0);
	XSync(Dpy, False);
}

void
drawbars(void) {
	struct Monitor *m;

	for(m = Mons; m; m = m->N)
		drawbar(m);
}

void
drawsquare(Bool filled, Bool empty, Bool invert, unsigned long col[ColLast]) {
	int x;

	XSetForeground(Dpy, dc.gc, col[invert ? ColBG : ColFG]);
	x = (dc.font.ascent + dc.font.descent + 2) / 4;
	if(filled)
		XFillRectangle(Dpy, dc.drawable, dc.gc, dc.x+1, dc.y+1, x+1, x+1);
	else if(empty)
		XDrawRectangle(Dpy, dc.drawable, dc.gc, dc.x+1, dc.y+1, x, x);
}

void
drawtext(const char *text, unsigned long col[ColLast], Bool invert) {
	char buf[256];
	int i, x, y, h, len, olen;

	XSetForeground(Dpy, dc.gc, col[invert ? ColFG : ColBG]);
	XFillRectangle(Dpy, dc.drawable, dc.gc, dc.x, dc.y, dc.w, dc.v);
	if(!text)
		return;
	olen = strlen(text);
	h = dc.font.ascent + dc.font.descent;
	y = dc.y + (dc.v / 2) - (h / 2) + dc.font.ascent;
	x = dc.x + (h / 2);
	/* shorten text if necessary */
	for(len = MIN(olen, sizeof buf); len && textnw(text, len) > dc.w - h; len--);
	if(!len)
		return;
	memcpy(buf, text, len);
	if(len < olen)
		for(i = len; i && i > len - 3; buf[--i] = '.');
	XSetForeground(Dpy, dc.gc, col[invert ? ColBG : ColFG]);
	if(dc.font.set)
		XmbDrawString(Dpy, dc.drawable, dc.font.set, dc.gc, x, y, buf, len);
	else
		XDrawString(Dpy, dc.drawable, dc.gc, x, y, buf, len);
}

void
enternotify(XEvent *e) {
	struct Client*c;
	struct Monitor *m;
	XCrossingEvent *ev = &e->xcrossing;

	if((ev->mode != NotifyNormal || ev->detail == NotifyInferior) && ev->window != root)
		return;
	c = wintoclient(ev->window);
	m = c ? c->Mon : wintomon(ev->window);
	if(m != Selmon) {
		unfocus(Selmon->Sel, True);
		Selmon = m;
	}
	else if(!c || c == Selmon->Sel)
		return;
	focus(c);
}

void
expose(XEvent *e) {
	struct Monitor *m;
	XExposeEvent *ev = &e->xexpose;

	if(ev->count == 0 && (m = wintomon(ev->window)))
		drawbar(m);
}

void
focus(struct Client*c) {
	if(!c || !ISVISIBLE(c))
		for(c = Selmon->Stack; c && !ISVISIBLE(c); c = c->snext);
	/* was if(Selmon->Sel) */
	if(Selmon->Sel && Selmon->Sel != c)
		unfocus(Selmon->Sel, False);
	if(c) {
		if(c->Mon != Selmon)
			Selmon = c->Mon;
		if(c->isurgent)
			clearurgent(c);
		detachstack(c);
		attachstack(c);
		grabbuttons(c, True);
		XSetWindowBorder(Dpy, c->win, dc.sel[ColBorder]);
		setfocus(c);
	}
	else
		XSetInputFocus(Dpy, root, RevertToPointerRoot, CurrentTime);
	Selmon->Sel = c;
	drawbars();
}

void
focusin(XEvent *e) { /* there are some broken focus acquiring Clients */
	XFocusChangeEvent *ev = &e->xfocus;

	if(Selmon->Sel && ev->window != Selmon->Sel->win)
		setfocus(Selmon->Sel);
}

void
focusmon(const Arg *arg) {
	struct Monitor *m;

	if(!Mons->N)
		return;
	if((m = dirtomon(arg->i)) == Selmon)
		return;
	unfocus(Selmon->Sel, True);
	Selmon = m;
	focus(NULL);
}

void
focusstack(const Arg *arg) {
	struct Client*c = NULL, *i;

	if(!Selmon->Sel)
		return;
	if(arg->i > 0) {
		for(c = Selmon->Sel->N; c && !ISVISIBLE(c); c = c->N);
		if(!c)
			for(c = Selmon->Clients; c && !ISVISIBLE(c); c = c->N);
	}
	else {
		for(i = Selmon->Clients; i != Selmon->Sel; i = i->N)
			if(ISVISIBLE(i))
				c = i;
		if(!c)
			for(; i; i = i->N)
				if(ISVISIBLE(i))
					c = i;
	}
	if(c) {
		focus(c);
		restack(Selmon);
	}
}

Atom
getatomprop(struct Client*c, Atom prop) {
	int di;
	unsigned long dl;
	unsigned char *p = NULL;
	Atom da, atom = None;

	if(XGetWindowProperty(Dpy, c->win, prop, 0L, sizeof atom, False, XA_ATOM,
	                      &da, &di, &dl, &dl, &p) == Success && p) {
		atom = *(Atom *)p;
		XFree(p);
	}
	return atom;
}

unsigned long
getcolor(const char *colstr) {
	Colormap cmap = DefaultColormap(Dpy, screen);
	XColor color;

	if(!XAllocNamedColor(Dpy, cmap, colstr, &color, &color))
		die("error, cannot allocate color '%s'\n", colstr);
	return color.pixel;
}

Bool
getrootptr(int *x, int *y) {
	int di;
	unsigned int dui;
	Window dummy;

	return XQueryPointer(Dpy, root, &dummy, &dummy, x, y, &di, &di, &dui);
}

long
getstate(Window w) {
	int format;
	long result = -1;
	unsigned char *p = NULL;
	unsigned long n, extra;
	Atom real;

	if(XGetWindowProperty(Dpy, w, wmatom[WMState], 0L, 2L, False, wmatom[WMState],
	                      &real, &format, &n, &extra, (unsigned char **)&p) != Success)
		return -1;
	if(n != 0)
		result = *p;
	XFree(p);
	return result;
}

Bool
gettextprop(Window w, Atom atom, char *text, unsigned int size) {
	char **list = NULL;
	int n;
	XTextProperty name;

	if(!text || size == 0)
		return False;
	text[0] = '\0';
	XGetTextProperty(Dpy, w, &name, atom);
	if(!name.nitems)
		return False;
	if(name.encoding == XA_STRING)
		strncpy(text, (char *)name.value, size - 1);
	else {
		if(XmbTextPropertyToTextList(Dpy, &name, &list, &n) >= Success && n > 0 && *list) {
			strncpy(text, *list, size - 1);
			XFreeStringList(list);
		}
	}
	text[size - 1] = '\0';
	XFree(name.value);
	return True;
}

void
grabbuttons(struct Client*c, Bool focused) {
	updatenumlockmask();
	{
		unsigned int i, j;
		unsigned int modifiers[] = { 0, LockMask, numlockmask, numlockmask|LockMask };
		XUngrabButton(Dpy, AnyButton, AnyModifier, c->win);
		if(focused) {
			for(i = 0; i < LENGTH(buttons); i++)
				if(buttons[i].click == ClkClientWin)
					for(j = 0; j < LENGTH(modifiers); j++)
						XGrabButton(Dpy, buttons[i].button,
						            buttons[i].mask | modifiers[j],
						            c->win, False, BUTTONMASK,
						            GrabModeAsync, GrabModeSync, None, None);
		}
		else
			XGrabButton(Dpy, AnyButton, AnyModifier, c->win, False,
			            BUTTONMASK, GrabModeAsync, GrabModeSync, None, None);
	}
}

void
grabkeys(void) {
	updatenumlockmask();
	{
		unsigned int i, j;
		unsigned int modifiers[] = { 0, LockMask, numlockmask, numlockmask|LockMask };
		KeyCode code;

		XUngrabKey(Dpy, AnyKey, AnyModifier, root);
		for(i = 0; i < LENGTH(keys); i++)
			if((code = XKeysymToKeycode(Dpy, keys[i].keysym)))
				for(j = 0; j < LENGTH(modifiers); j++)
					XGrabKey(Dpy, code, keys[i].mod | modifiers[j], root,
						 True, GrabModeAsync, GrabModeAsync);
	}
}

void
incnmaster(const Arg *arg) {
	Selmon->nmaster = MAX(Selmon->nmaster + arg->i, 0);
	arrange(Selmon);
}

void
initfont(const char *fontstr) {
	char *def, **missing;
	int n;

	dc.font.set = XCreateFontSet(Dpy, fontstr, &missing, &n, &def);
	if(missing) {
		while(n--)
			fprintf(stderr, "mwm: missing fontset: %s\n", missing[n]);
		XFreeStringList(missing);
	}
	if(dc.font.set) {
		XFontStruct **xfonts;
		char **font_names;

		dc.font.ascent = dc.font.descent = 0;
		XExtentsOfFontSet(dc.font.set);
		n = XFontsOfFontSet(dc.font.set, &xfonts, &font_names);
		while(n--) {
			dc.font.ascent = MAX(dc.font.ascent, (*xfonts)->ascent);
			dc.font.descent = MAX(dc.font.descent,(*xfonts)->descent);
			xfonts++;
		}
	}
	else {
		if(!(dc.font.xfont = XLoadQueryFont(Dpy, fontstr))
		&& !(dc.font.xfont = XLoadQueryFont(Dpy, "fixed")))
			die("error, cannot load font: '%s'\n", fontstr);
		dc.font.ascent = dc.font.xfont->ascent;
		dc.font.descent = dc.font.xfont->descent;
	}
	dc.font.height = dc.font.ascent + dc.font.descent;
}

#ifdef XINERAMA
static Bool
isuniquegeom(XineramaScreenInfo *unique, size_t n, XineramaScreenInfo *info) {
	while(n--)
		if(unique[n].x_org == info->x_org && unique[n].y_org == info->y_org
		&& unique[n].width == info->width && unique[n].height == info->height)
			return False;
	return True;
}
#endif /* XINERAMA */

void
keypress(XEvent *e) {
	unsigned int i;
	KeySym keysym;
	XKeyEvent *ev;

	ev = &e->xkey;
	keysym = XKeycodeToKeysym(Dpy, (KeyCode)ev->keycode, 0);
	for(i = 0; i < LENGTH(keys); i++)
		if(keysym == keys[i].keysym
		&& CLEANMASK(keys[i].mod) == CLEANMASK(ev->state)
		&& keys[i].func)
			keys[i].func(&(keys[i].arg));
}

void
killclient(const Arg*arg)
{arg;
 if(!Selmon->Sel)
  return;
 if(!sendevent(Selmon->Sel,wmatom[WMDelete]))
 {XGrabServer(Dpy);
  XSetErrorHandler(xerrordummy);
  XSetCloseDownMode(Dpy,DestroyAll);
  XKillClient(Dpy,Selmon->Sel->win);
  XSync(Dpy,False);
  XSetErrorHandler(xerror);
  XUngrabServer(Dpy);
 }
}

void
manage(Window w, XWindowAttributes *wa) {
	struct Client*c,*t=NULL;
	Window trans = None;
	XWindowChanges wc;

	if(!(c = calloc(1, sizeof(struct Client))))
		die("fatal: could not malloc() %u bytes\n", sizeof(struct Client));
	c->win = w;
	updatetitle(c);
	if(XGetTransientForHint(Dpy, w, &trans) && (t = wintoclient(trans))) {
		c->Mon = t->Mon;
		c->tags = t->tags;
	}
	else {
		c->Mon = Selmon;
		applyrules(c);
	}
	/* geometry */
	c->y = c->oldy = wa->y;
	c->x = c->oldx = wa->x;
	c->v = c->oldv = wa->height;
	c->w = c->oldw = wa->width;
	c->oldbw = wa->border_width;

	if(c->x + WIDTH(c) > c->Mon->mx + c->Mon->mw)
		c->x = c->Mon->mx + c->Mon->mw - WIDTH(c);
	if(c->y + VERTH(c) > c->Mon->my + c->Mon->mv)
		c->y = c->Mon->my + c->Mon->mv - VERTH(c);
	c->x = MAX(c->x, c->Mon->mx);
	/* only fix client y-offset, if the client center might cover the bar */
	c->y = MAX(c->y, ((c->Mon->by == c->Mon->my) && (c->x + (c->w / 2) >= c->Mon->wx)
	           && (c->x + (c->w / 2) < c->Mon->wx + c->Mon->ww)) ? bh : c->Mon->my);
	c->bw = borderpx;

	wc.border_width = c->bw;
	XConfigureWindow(Dpy, w, CWBorderWidth, &wc);
	XSetWindowBorder(Dpy, w, dc.norm[ColBorder]);
	configure(c); /* propagates border_width, if size doesn't change */
	updatewindowtype(c);
	updatesizehints(c);
	updatewmhints(c);
	XSelectInput(Dpy, w, EnterWindowMask|FocusChangeMask|PropertyChangeMask|StructureNotifyMask);
	grabbuttons(c, False);
	if(!c->isfloating)
		c->isfloating = c->oldstate = trans != None || c->isfixed;
	if(c->isfloating)
		XRaiseWindow(Dpy, c->win);
	attach(c);
	attachstack(c);
	XMoveResizeWindow(Dpy, c->win, c->x + 2 * sw, c->y, c->w, c->v); /* some windows require this */
	setclientstate(c, NormalState);
	if (c->Mon == Selmon)
		unfocus(Selmon->Sel, False);
	c->Mon->Sel = c;
	arrange(c->Mon);
	XMapWindow(Dpy, c->win);
	focus(NULL);
}

void
mappingnotify(XEvent *e) {
	XMappingEvent *ev = &e->xmapping;

	XRefreshKeyboardMapping(ev);
	if(ev->request == MappingKeyboard)
		grabkeys();
}

void
maprequest(XEvent *e) {
	static XWindowAttributes wa;
	XMapRequestEvent *ev = &e->xmaprequest;

	if(!XGetWindowAttributes(Dpy, ev->window, &wa))
		return;
	if(wa.override_redirect)
		return;
	if(!wintoclient(ev->window))
		manage(ev->window, &wa);
}

void
monocle(struct Monitor *m) {
	unsigned int n = 0;
	struct Client *c;

	for(c = m->Clients; c; c = c->N)
		if(ISVISIBLE(c))
			n++;
	if(n > 0) /* override layout symbol */
		snprintf(m->ltsymbol, sizeof m->ltsymbol, "[%d]", n);
	for(c = nexttiled(m->Clients); c; c = nexttiled(c->N))
		resize(c, m->wx, m->wy, m->ww - 2 * c->bw, m->wv - 2 * c->bw, False);
}

void
motionnotify(XEvent *e) {
	static struct Monitor*Mon = NULL;
	struct Monitor *m;
	XMotionEvent *ev = &e->xmotion;

	if(ev->window != root)
		return;
	if((m = recttomon(ev->x_root, ev->y_root, 1, 1)) != Mon && Mon) {
		Selmon = m;
		focus(NULL);
	}
	Mon = m;
}

void
movemouse(const Arg *arg)
{arg;
 int x, y, ocx, ocy, nx, ny;
 struct Client *c;
 struct Monitor *m;
 XEvent ev;

 if(!(c = Selmon->Sel))
  return;
 restack(Selmon);
 ocx = c->x;
 ocy = c->y;
 if(XGrabPointer(Dpy, root, False, MOUSEMASK, GrabModeAsync, GrabModeAsync,
    None, cursor[CurMove], CurrentTime) != GrabSuccess)
  return;
 if(!getrootptr(&x, &y))
  return;
 do
 {XMaskEvent(Dpy, MOUSEMASK|ExposureMask|SubstructureRedirectMask, &ev);
  switch(ev.type)
  {case ConfigureRequest:
   case Expose:
   case MapRequest:
    handler[ev.type](&ev);
    break;
   case MotionNotify:
    nx = ocx + (ev.xmotion.x - x);
    ny = ocy + (ev.xmotion.y - y);
    if(nx >= Selmon->wx && nx <= Selmon->wx + Selmon->ww
    && ny >= Selmon->wy && ny <= Selmon->wy + Selmon->wv)
    {if(abs(Selmon->wx - nx) < snap)
      nx = Selmon->wx;
     else
      if(abs((Selmon->wx + Selmon->ww) - (nx + WIDTH(c))) < snap)
       nx = Selmon->wx + Selmon->ww - WIDTH(c);
      if(abs(Selmon->wy - ny) < snap)
       ny = Selmon->wy;
     else
      if(abs((Selmon->wy + Selmon->wv) - (ny + VERTH(c))) < snap)
       ny = Selmon->wy + Selmon->wv - VERTH(c);
      if(!c->isfloating && Selmon->Lt[Selmon->sellt]->arrange
      && (abs(nx - c->x) > snap || abs(ny - c->y) > snap))
       togglefloating(NULL);
    }
    if(!Selmon->Lt[Selmon->sellt]->arrange || c->isfloating)
     resize(c, nx, ny, c->w, c->v, True);
    break;
  }
 }while(ev.type != ButtonRelease);
 XUngrabPointer(Dpy, CurrentTime);
 if((m = recttomon(c->x, c->y, c->w, c->v)) != Selmon)
 {sendmon(c, m);
  Selmon = m;
  focus(NULL);
 }
}

struct Client *
nexttiled(struct Client *c) {
	for(; c && (c->isfloating || !ISVISIBLE(c)); c = c->N);
	return c;
}

void
pop(struct Client *c) {
	detach(c);
	attach(c);
	focus(c);
	arrange(c->Mon);
}

void
propertynotify(XEvent *e) {
	struct Client *c;
	Window trans;
	XPropertyEvent *ev = &e->xproperty;

	if((ev->window == root) && (ev->atom == XA_WM_NAME))
		updatestatus();
	else if(ev->state == PropertyDelete)
		return; /* ignore */
	else if((c = wintoclient(ev->window))) {
		switch(ev->atom) {
		default: break;
		case XA_WM_TRANSIENT_FOR:
			if(!c->isfloating && (XGetTransientForHint(Dpy, c->win, &trans)) &&
			   (c->isfloating = (wintoclient(trans)) != NULL))
				arrange(c->Mon);
			break;
		case XA_WM_NORMAL_HINTS:
			updatesizehints(c);
			break;
		case XA_WM_HINTS:
			updatewmhints(c);
			drawbars();
			break;
		}
		if(ev->atom == XA_WM_NAME || ev->atom == netatom[NetWMName]) {
			updatetitle(c);
			if(c == c->Mon->Sel)
				drawbar(c->Mon);
		}
		if(ev->atom == netatom[NetWMWindowType])
			updatewindowtype(c);
	}
}

void
quit(const Arg*arg)
{arg;
 running=False;
}

struct Monitor *
recttomon(int x, int y, int w, int h) {
	struct Monitor *m, *r = Selmon;
	int a, area = 0;

	for(m = Mons; m; m = m->N)
		if((a = INTERSECT(x, y, w, h, m)) > area) {
			area = a;
			r = m;
		}
	return r;
}

void
resize(struct Client *c, int x, int y, int w, int h, Bool interact) {
	if(applysizehints(c, &x, &y, &w, &h, interact))
		resizeclient(c, x, y, w, h);
}

void
resizeclient(struct Client *c, int x, int y, int w, int h) {
	XWindowChanges wc;

	c->oldy = c->y; c->y = wc.y = y;
	c->oldx = c->x; c->x = wc.x = x;
	c->oldv = c->v; c->v = wc.height = v;
	c->oldw = c->w; c->w = wc.width = w;
	wc.border_width = c->bw;
	XConfigureWindow(Dpy, c->win, CWX|CWY|CWWidth|CWHeight|CWBorderWidth, &wc);
	configure(c);
	XSync(Dpy, False);
}

void
resizemouse(const Arg*arg)
{XEvent ev;
 struct Client *c;
 struct Monitor *m;
 int ocx, ocy;
 int nw, nv;
 arg;

 if(!(c = Selmon->Sel))
  return;
 restack(Selmon);
 ocx = c->x;
 ocy = c->y;
 if(XGrabPointer(Dpy, root, False, MOUSEMASK, GrabModeAsync, GrabModeAsync,
    None, cursor[CurResize], CurrentTime) != GrabSuccess)
  return;
 XWarpPointer(Dpy, None, c->win, 0, 0, 0, 0, c->w + c->bw - 1, c->v + c->bw - 1);
 do
 {XMaskEvent(Dpy, MOUSEMASK|ExposureMask|SubstructureRedirectMask, &ev);
  switch(ev.type)
  {case ConfigureRequest:
   case Expose:
   case MapRequest:
    handler[ev.type](&ev);
    break;
   case MotionNotify:
    nv = MAX(ev.xmotion.y - ocy - 2 * c->bw + 1, 1);
    nw = MAX(ev.xmotion.x - ocx - 2 * c->bw + 1, 1);
    if(c->Mon->wy+nv>=Selmon->wy&&c->Mon->wy+nv<=Selmon->wy+Selmon->wv
    && c->Mon->wx+nw>=Selmon->wx&&c->Mon->wx+nw<=Selmon->wx+Selmon->ww)
    {if(!c->isfloating&&Selmon->Lt[Selmon->sellt]->arrange
     &&(abs(nv-c->v)>snap||abs(nw-c->w)>snap))
      togglefloating(NULL);
    }
    if(!Selmon->Lt[Selmon->sellt]->arrange || c->isfloating)
     resize(c, c->x, c->y, nw, nv, True);
    break;
  }
 }while(ev.type != ButtonRelease);
 XWarpPointer(Dpy, None, c->win, 0, 0, 0, 0, c->w + c->bw - 1, c->v + c->bw - 1);
 XUngrabPointer(Dpy, CurrentTime);
 while(XCheckMaskEvent(Dpy, EnterWindowMask, &ev));
 if((m = recttomon(c->x, c->y, c->w, c->v)) != Selmon)
 {sendmon(c, m);
  Selmon = m;
  focus(NULL);
 }
}

void
restack(struct Monitor*m)
{XEvent         ev;
 XWindowChanges wc;
 struct Client         *c;
 drawbar(m);
 if(!m->Sel)
  return;
 if(m->Sel->isfloating||!m->Lt[m->sellt]->arrange)
  XRaiseWindow(Dpy,m->Sel->win);
 if(m->Lt[m->sellt]->arrange)
 {wc.stack_mode=Below;
  wc.sibling=m->barwin;
  for(c=m->Stack;c;c=c->snext) /*,//// s/;c;/;;/ possible? */
   if(!c->isfloating&&ISVISIBLE(c))
   {XConfigureWindow(Dpy,c->win,CWSibling|CWStackMode,&wc);
    wc.sibling=c->win;
   }
 }
 XSync(Dpy,False);
 while(XCheckMaskEvent(Dpy,EnterWindowMask,&ev));
}

void
scan(void) {
	unsigned int i, num;
	Window d1, d2, *wins = NULL;
	XWindowAttributes wa;

	if(XQueryTree(Dpy, root, &d1, &d2, &wins, &num)) {
		for(i = 0; i < num; i++) {
			if(!XGetWindowAttributes(Dpy, wins[i], &wa)
			|| wa.override_redirect || XGetTransientForHint(Dpy, wins[i], &d1))
				continue;
			if(wa.map_state == IsViewable || getstate(wins[i]) == IconicState)
				manage(wins[i], &wa);
		}
		for(i = 0; i < num; i++) { /* now the transients */
			if(!XGetWindowAttributes(Dpy, wins[i], &wa))
				continue;
			if(XGetTransientForHint(Dpy, wins[i], &d1)
			&& (wa.map_state == IsViewable || getstate(wins[i]) == IconicState))
				manage(wins[i], &wa);
		}
		if(wins)
			XFree(wins);
	}
}

void
sendmon(struct Client *c, struct Monitor *m) {
	if(c->Mon == m)
		return;
	unfocus(c, True);
	detach(c);
	detachstack(c);
	c->Mon = m;
	c->tags = m->tagset[m->seltags]; /* assign tags of target monitor */
	attach(c);
	attachstack(c);
	focus(NULL);
	arrange(NULL);
}

void
setclientstate(struct Client *c, long state) {
	long data[] = { state, None };

	XChangeProperty(Dpy, c->win, wmatom[WMState], wmatom[WMState], 32,
			PropModeReplace, (unsigned char *)data, 2);
}

Bool
sendevent(struct Client *c, Atom proto) {
	int n;
	Atom *protocols;
	Bool exists = False;
	XEvent ev;

	if(XGetWMProtocols(Dpy, c->win, &protocols, &n)) {
		while(!exists && n--)
			exists = protocols[n] == proto;
		XFree(protocols);
	}
	if(exists) {
		ev.type = ClientMessage;
		ev.xclient.window = c->win;
		ev.xclient.message_type = wmatom[WMProtocols];
		ev.xclient.format = 32;
		ev.xclient.data.l[0] = proto;
		ev.xclient.data.l[1] = CurrentTime;
		XSendEvent(Dpy, c->win, False, NoEventMask, &ev);
	}
	return exists;
}

void
setfocus(struct Client *c) {
	if(!c->neverfocus)
		XSetInputFocus(Dpy, c->win, RevertToPointerRoot, CurrentTime);
	sendevent(c, wmatom[WMTakeFocus]);
}

void
setfullscreen(struct Client *c, Bool fullscreen) {
	if(fullscreen) {
		XChangeProperty(Dpy, c->win, netatom[NetWMState], XA_ATOM, 32,
		                PropModeReplace, (unsigned char*)&netatom[NetWMFullscreen], 1);
		c->isfullscreen = True;
		c->oldstate = c->isfloating;
		c->oldbw = c->bw;
		c->bw = 0;
		c->isfloating = True;
		resizeclient(c, c->Mon->mx, c->Mon->my, c->Mon->mw, c->Mon->mv);
		XRaiseWindow(Dpy, c->win);
	}
	else {
		XChangeProperty(Dpy, c->win, netatom[NetWMState], XA_ATOM, 32,
		                PropModeReplace, (unsigned char*)0, 0);
		c->isfullscreen = False;
		c->isfloating = c->oldstate;
		c->bw = c->oldbw;
		c->y = c->oldy;
		c->x = c->oldx;
		c->v = c->oldv;
		c->w = c->oldw;
		resizeclient(c, c->x, c->y, c->w, c->v);
		arrange(c->Mon);
	}
}

void
setlayout(const Arg *arg) {
	if(!arg || !arg->P || arg->P != Selmon->Lt[Selmon->sellt])
		Selmon->sellt ^= 1;
	if(arg && arg->P)
		Selmon->Lt[Selmon->sellt] = (struct Layout *)arg->P;
	strncpy(Selmon->ltsymbol, Selmon->Lt[Selmon->sellt]->symbol, sizeof Selmon->ltsymbol);
	if(Selmon->Sel)
		arrange(Selmon);
	else
		drawbar(Selmon);
}

/* arg > 1.0 will set mfact absolutly */
void
setmfact(const Arg *arg) {
	float f;

	if(!arg || !Selmon->Lt[Selmon->sellt]->arrange)
		return;
	f = arg->f < 1.0 ? arg->f + Selmon->mfact : arg->f - 1.0;
	if(f < 0.1 || f > 0.9)
		return;
	Selmon->mfact = f;
	arrange(Selmon);
}

void
setup(void) {
	XSetWindowAttributes wa;

	/* clean up any zombies immediately */
	sigchld(0);

	/* init screen */
	screen = DefaultScreen(Dpy);
	root = RootWindow(Dpy, screen);
	initfont(font);
	sw = DisplayWidth(Dpy, screen);
	sv = DisplayHeight(Dpy, screen);
	bh = dc.v = dc.font.height + 2;
	updategeom();
	/* init atoms */
	wmatom[WMProtocols] = XInternAtom(Dpy, "WM_PROTOCOLS", False);
	wmatom[WMDelete] = XInternAtom(Dpy, "WM_DELETE_WINDOW", False);
	wmatom[WMState] = XInternAtom(Dpy, "WM_STATE", False);
	wmatom[WMTakeFocus] = XInternAtom(Dpy, "WM_TAKE_FOCUS", False);
	netatom[NetActiveWindow] = XInternAtom(Dpy, "_NET_ACTIVE_WINDOW", False);
	netatom[NetSupported] = XInternAtom(Dpy, "_NET_SUPPORTED", False);
	netatom[NetWMName] = XInternAtom(Dpy, "_NET_WM_NAME", False);
	netatom[NetWMState] = XInternAtom(Dpy, "_NET_WM_STATE", False);
	netatom[NetWMFullscreen] = XInternAtom(Dpy, "_NET_WM_STATE_FULLSCREEN", False);
	netatom[NetWMWindowType] = XInternAtom(Dpy, "_NET_WM_WINDOW_TYPE", False);
	netatom[NetWMWindowTypeDialog] = XInternAtom(Dpy, "_NET_WM_WINDOW_TYPE_DIALOG", False);
	/* init cursors */
	cursor[CurNormal] = XCreateFontCursor(Dpy, XC_left_ptr);
	cursor[CurResize] = XCreateFontCursor(Dpy, XC_sizing);
	cursor[CurMove] = XCreateFontCursor(Dpy, XC_fleur);
	/* init appearance */
	dc.norm[ColBorder] = getcolor(normbordercolor);
	dc.norm[ColBG] = getcolor(normbgcolor);
	dc.norm[ColFG] = getcolor(normfgcolor);
	dc.sel[ColBorder] = getcolor(selbordercolor);
	dc.sel[ColBG] = getcolor(selbgcolor);
	dc.sel[ColFG] = getcolor(selfgcolor);
	dc.drawable = XCreatePixmap(Dpy, root, DisplayWidth(Dpy, screen), bh, DefaultDepth(Dpy, screen));
	dc.gc = XCreateGC(Dpy, root, 0, NULL);
	XSetLineAttributes(Dpy, dc.gc, 1, LineSolid, CapButt, JoinMiter);
	if(!dc.font.set)
		XSetFont(Dpy, dc.gc, dc.font.xfont->fid);
	/* init bars */
	updatebars();
	updatestatus();
	/* EWMH support per view */
	XChangeProperty(Dpy, root, netatom[NetSupported], XA_ATOM, 32,
			PropModeReplace, (unsigned char *) netatom, NetLast);
	/* select for events */
	wa.cursor = cursor[CurNormal];
	wa.event_mask = SubstructureRedirectMask|SubstructureNotifyMask|ButtonPressMask|PointerMotionMask
	                |EnterWindowMask|LeaveWindowMask|StructureNotifyMask|PropertyChangeMask;
	XChangeWindowAttributes(Dpy, root, CWEventMask|CWCursor, &wa);
	XSelectInput(Dpy, root, wa.event_mask);
	grabkeys();
}

void
showhide(struct Client *c) {
	if(!c)
		return;
	if(ISVISIBLE(c)) { /* show Clients top down */
		XMoveWindow(Dpy, c->win, c->x, c->y);
		if((!c->Mon->Lt[c->Mon->sellt]->arrange || c->isfloating) && !c->isfullscreen)
			resize(c, c->x, c->y, c->w, c->v, False);
		showhide(c->snext);
	}
	else { /* hide Clients bottom up */
		showhide(c->snext);
		XMoveWindow(Dpy, c->win, WIDTH(c) * -2, c->y);
	}
}

void
sigchld(int unused)
{unused;
 if(signal(SIGCHLD,sigchld)==SIG_ERR)
  die("Can't install SIGCHLD handler");
 while(0<waitpid(-1,NULL,WNOHANG));
}

void
spawn(const Arg *arg) {
	if(fork() == 0) {
		if(Dpy)
			close(ConnectionNumber(Dpy));
		setsid();
		execvp(((char **)arg->P)[0], (char **)arg->P);
		fprintf(stderr, "mwm: execvp %s", ((char **)arg->P)[0]);
		perror(" failed");
		exit(EXIT_SUCCESS);
	}
}

void
tag(const Arg *arg) {
	if(Selmon->Sel && arg->ui & TAGMASK) {
		Selmon->Sel->tags = arg->ui & TAGMASK;
		focus(NULL);
		arrange(Selmon);
	}
}

void
tagmon(const Arg *arg) {
	if(!Selmon->Sel || !Mons->N)
		return;
	sendmon(Selmon->Sel, dirtomon(arg->i));
}

int
textnw(const char *text, unsigned int len) {
	XRectangle r;

	if(dc.font.set) {
		XmbTextExtents(dc.font.set, text, len, NULL, &r);
		return r.width;
	}
	return XTextWidth(dc.font.xfont, text, len);
}

void
tile(struct Monitor *m) {
	unsigned int i, n, h, mw, my, ty;
	struct Client *c;

	for(n = 0, c = nexttiled(m->Clients); c; c = nexttiled(c->N), n++);
	if(n == 0)
		return;

	if(n > m->nmaster)
		mw = m->nmaster ? m->ww * m->mfact : 0;
	else
		mw = m->ww;
	for(i = my = ty = 0, c = nexttiled(m->Clients); c; c = nexttiled(c->N), i++)
		if(i < m->nmaster) {
			h = (m->wv - my) / (MIN(n, m->nmaster) - i);
			resize(c, m->wx, m->wy + my, mw - (2*c->bw), h - (2*c->bw), False);
			my += VERTH(c);
		}
		else {
			h = (m->wv - ty) / (n - i);
			resize(c, m->wx + mw, m->wy + ty, m->ww - mw - (2*c->bw), h - (2*c->bw), False);
			ty += VERTH(c);
		}
}

void
togglebar(const Arg*arg)
{arg;
 Selmon->showbar=!Selmon->showbar;
 updatebarpos(Selmon);
 XMoveResizeWindow(Dpy,Selmon->barwin,Selmon->wx,Selmon->by,Selmon->ww,bh);
 arrange(Selmon);
}

void
togglefloating(const Arg*arg)
{arg;
 if(!Selmon->Sel)
  return;
 Selmon->Sel->isfloating = !Selmon->Sel->isfloating || Selmon->Sel->isfixed;
 if(Selmon->Sel->isfloating)
  resize(Selmon->Sel, Selmon->Sel->x, Selmon->Sel->y,
         Selmon->Sel->w, Selmon->Sel->v, False);
 arrange(Selmon);
}

void
toggletag(const Arg*arg)
{unsigned int newtags;
 if(!Selmon->Sel)
  return;
 newtags=Selmon->Sel->tags^(arg->ui&TAGMASK);
 if(newtags)
 {Selmon->Sel->tags=newtags;
  focus(NULL);
  arrange(Selmon);
 }
}

void
toggleview(const Arg*arg)
{unsigned int newtagset=Selmon->tagset[Selmon->seltags]^(arg->ui&TAGMASK);
 if(newtagset)
 {Selmon->tagset[Selmon->seltags]=newtagset;
  focus(NULL);
  arrange(Selmon);
 }
}

void
unfocus(struct Client *c, Bool setfocus) {
	if(!c)
		return;
	grabbuttons(c, False);
	XSetWindowBorder(Dpy, c->win, dc.norm[ColBorder]);
	if(setfocus)
		XSetInputFocus(Dpy, root, RevertToPointerRoot, CurrentTime);
}

void
unmanage(struct Client *c, Bool destroyed) {
	struct Monitor *m = c->Mon;
	XWindowChanges wc;

	/* The server grab construct avoids race conditions. */
	detach(c);
	detachstack(c);
	if(!destroyed) {
		wc.border_width = c->oldbw;
		XGrabServer(Dpy);
		XSetErrorHandler(xerrordummy);
		XConfigureWindow(Dpy, c->win, CWBorderWidth, &wc); /* restore border */
		XUngrabButton(Dpy, AnyButton, AnyModifier, c->win);
		setclientstate(c, WithdrawnState);
		XSync(Dpy, False);
		XSetErrorHandler(xerror);
		XUngrabServer(Dpy);
	}
	free(c);
	focus(NULL);
	arrange(m);
}

void
unmapnotify(XEvent *e) {
	struct Client *c;
	XUnmapEvent *ev = &e->xunmap;

	if((c = wintoclient(ev->window))) {
		if(ev->send_event)
			setclientstate(c, WithdrawnState);
		else
			unmanage(c, False);
	}
}

void
updatebars(void) {
	struct Monitor *m;
	XSetWindowAttributes wa = {
		.override_redirect = True,
		.background_pixmap = ParentRelative,
		.event_mask = ButtonPressMask|ExposureMask
	};
	for(m = Mons; m; m = m->N) {
		m->barwin = XCreateWindow(Dpy, root, m->wx, m->by, m->ww, bh, 0, DefaultDepth(Dpy, screen),
		                          CopyFromParent, DefaultVisual(Dpy, screen),
		                          CWOverrideRedirect|CWBackPixmap|CWEventMask, &wa);
		XDefineCursor(Dpy, m->barwin, cursor[CurNormal]);
		XMapRaised(Dpy, m->barwin);
	}
}

void
updatebarpos(struct Monitor *m) {
	m->wy = m->my;
	m->wv = m->mv;
	if(m->showbar) {
		m->wv -= bh;
		m->by = m->topbar ? m->wy : m->wy + m->wv;
		m->wy = m->topbar ? m->wy + bh : m->wy;
	}
	else
		m->by = -bh;
}

Bool
updategeom(void) {
	Bool dirty = False;

#ifdef XINERAMA
	if(XineramaIsActive(Dpy)) {
		int i, j, n, nn;
		struct Client *c;
		struct Monitor *m;
		XineramaScreenInfo *info = XineramaQueryScreens(Dpy, &nn);
		XineramaScreenInfo *unique = NULL;

		for(n = 0, m = Mons; m; m = m->N, n++);
		/* only consider unique geometries as separate screens */
		if(!(unique = (XineramaScreenInfo *)malloc(sizeof(XineramaScreenInfo) * nn)))
			die("fatal: could not malloc() %u bytes\n", sizeof(XineramaScreenInfo) * nn);
		for(i = 0, j = 0; i < nn; i++)
			if(isuniquegeom(unique, j, &info[i]))
				memcpy(&unique[j++], &info[i], sizeof(XineramaScreenInfo));
		XFree(info);
		nn = j;
		if(n <= nn) {
			for(i = 0; i < (nn - n); i++) { /* new monitors available */
				for(m = Mons; m && m->N; m = m->N);
				if(m)
					m->N = createmon();
				else
					Mons = createmon();
			}
			for(i = 0, m = Mons; i < nn && m; m = m->N, i++)
				if(i >= n
				|| (unique[i].x_org != m->mx || unique[i].y_org != m->my
				    || unique[i].width != m->mw || unique[i].height != m->mv))
				{
					dirty = True;
					m->num = i;
					m->mx = m->wx = unique[i].x_org;
					m->my = m->wy = unique[i].y_org;
					m->mw = m->ww = unique[i].width;
					m->mv = m->wv = unique[i].height;
					updatebarpos(m);
				}
		}
		else { /* less monitors available nn < n */
			for(i = nn; i < n; i++) {
				for(m = Mons; m && m->N; m = m->N);
				while(m->Clients) {
					dirty = True;
					c = m->Clients;
					m->Clients = c->N;
					detachstack(c);
					c->Mon = Mons;
					attach(c);
					attachstack(c);
				}
				if(m == Selmon)
					Selmon = Mons;
				cleanupmon(m);
			}
		}
		free(unique);
	}
	else
#endif /* XINERAMA */
	/* default monitor setup */
	{
		if(!Mons)
			Mons = createmon();
		if(Mons->mw != sw || Mons->mv != sv) {
			dirty = True;
			Mons->mw = Mons->ww = sw;
			Mons->mv = Mons->wv = sv;
			updatebarpos(Mons);
		}
	}
	if(dirty) {
		Selmon = Mons;
		Selmon = wintomon(root);
	}
	return dirty;
}

void
updatenumlockmask(void) {
	unsigned int i, j;
	XModifierKeymap *modmap;

	numlockmask = 0;
	modmap = XGetModifierMapping(Dpy);
	for(i = 0; i < 8; i++)
		for(j = 0; j < modmap->max_keypermod; j++)
			if(modmap->modifiermap[i * modmap->max_keypermod + j]
			   == XKeysymToKeycode(Dpy, XK_Num_Lock))
				numlockmask = (1 << i);
	XFreeModifiermap(modmap);
}

void
updatesizehints(struct Client *c) {
	long msize;
	XSizeHints size;

	if(!XGetWMNormalHints(Dpy, c->win, &size, &msize))
		/* size is uninitialized, ensure that size.flags aren't used */
		size.flags = PSize;
	if(size.flags & PBaseSize) {
		c->basew = size.base_width;
		c->basev = size.base_height;
	}
	else if(size.flags & PMinSize) {
		c->basew = size.min_width;
		c->basev = size.min_height;
	}
	else
		c->basew = c->basev = 0;
	if(size.flags & PResizeInc) {
		c->incw = size.width_inc;
		c->incv = size.height_inc;
	}
	else
		c->incw = c->incv = 0;
	if(size.flags & PMaxSize) {
		c->maxw = size.max_width;
		c->maxv = size.max_height;
	}
	else
		c->maxw = c->maxv = 0;
	if(size.flags & PMinSize) {
		c->minw = size.min_width;
		c->minv = size.min_height;
	}
	else if(size.flags & PBaseSize) {
		c->minw = size.base_width;
		c->minv = size.base_height;
	}
	else
		c->minw = c->minv = 0;
	if(size.flags & PAspect) {
		c->mina = (float)size.min_aspect.y / size.min_aspect.x;
		c->maxa = (float)size.max_aspect.x / size.max_aspect.y;
	}
	else
		c->maxa = c->mina = 0.0;
	c->isfixed = (c->maxw && c->minw && c->maxv && c->minv
	             && c->maxw == c->minw && c->maxv == c->minv);
}

void
updatetitle(struct Client *c) {
	if(!gettextprop(c->win, netatom[NetWMName], c->name, sizeof c->name))
		gettextprop(c->win, XA_WM_NAME, c->name, sizeof c->name);
	if(c->name[0] == '\0') /* hack to mark broken Clients */
		strcpy(c->name, broken);
}

void
updatestatus(void) {
	if(!gettextprop(root, XA_WM_NAME, stext, sizeof(stext)))
		strcpy(stext, "mwm-"VERSION);
	drawbar(Selmon);
}

void
updatewindowtype(struct Client *c) {
	Atom state = getatomprop(c, netatom[NetWMState]);
	Atom wtype = getatomprop(c, netatom[NetWMWindowType]);

	if(state == netatom[NetWMFullscreen])
		setfullscreen(c, True);

	if(wtype == netatom[NetWMWindowTypeDialog])
		c->isfloating = True;
}

void
updatewmhints(struct Client *c) {
	XWMHints *wmh;

	if((wmh = XGetWMHints(Dpy, c->win))) {
		if(c == Selmon->Sel && wmh->flags & XUrgencyHint) {
			wmh->flags &= ~XUrgencyHint;
			XSetWMHints(Dpy, c->win, wmh);
		}
		else
			c->isurgent = (wmh->flags & XUrgencyHint) ? True : False;
		if(wmh->flags & InputHint)
			c->neverfocus = !wmh->input;
		else
			c->neverfocus = False;
		XFree(wmh);
	}
}

void
view(const Arg *arg) {
	if((arg->ui & TAGMASK) == Selmon->tagset[Selmon->seltags])
		return;
	Selmon->seltags ^= 1; /* toggle Sel tagset */
	if(arg->ui & TAGMASK)
		Selmon->tagset[Selmon->seltags] = arg->ui & TAGMASK;
	focus(NULL);
	arrange(Selmon);
}

struct Client *
wintoclient(Window w) {
	struct Client *c;
	struct Monitor *m;

	for(m = Mons; m; m = m->N)
		for(c = m->Clients; c; c = c->N)
			if(c->win == w)
				return c;
	return NULL;
}

struct Monitor *
wintomon(Window w) {
	int x, y;
	struct Client *c;
	struct Monitor *m;

	if(w == root && getrootptr(&x, &y))
		return recttomon(x, y, 1, 1);
	for(m = Mons; m; m = m->N)
		if(w == m->barwin)
			return m;
	if((c = wintoclient(w)))
		return c->Mon;
	return Selmon;
}

/* There's no way to check accesses to destroyed windows, thus those cases are
 * ignored (especially on UnmapNotify's).  Other types of errors call Xlibs
 * default error handler, which may call exit.  */
int
xerror(Display *Dpy, XErrorEvent *ee) {
	if(ee->error_code == BadWindow
	|| (ee->request_code == X_SetInputFocus && ee->error_code == BadMatch)
	|| (ee->request_code == X_PolyText8 && ee->error_code == BadDrawable)
	|| (ee->request_code == X_PolyFillRectangle && ee->error_code == BadDrawable)
	|| (ee->request_code == X_PolySegment && ee->error_code == BadDrawable)
	|| (ee->request_code == X_ConfigureWindow && ee->error_code == BadMatch)
	|| (ee->request_code == X_GrabButton && ee->error_code == BadAccess)
	|| (ee->request_code == X_GrabKey && ee->error_code == BadAccess)
	|| (ee->request_code == X_CopyArea && ee->error_code == BadDrawable))
		return 0;
	fprintf(stderr, "mwm: fatal error: request code=%d, error code=%d\n",
			ee->request_code, ee->error_code);
	return xerrorxlib(Dpy, ee); /* may call exit */
}

int
xerrordummy(Display *Dpy, XErrorEvent *ee) {
	return 0;
}

/* Startup Error handler to check if another window manager
 * is already running. */
int
xerrorstart(Display *Dpy, XErrorEvent *ee) {
	die("mwm: another window manager is already running\n");
	return -1;
}

void
zoom(const Arg*arg)
{struct Client*c=Selmon->Sel;
 arg;
 if(!Selmon->Lt[Selmon->sellt]->arrange
 ||(Selmon->Sel&&Selmon->Sel->isfloating))
  return;
 if(c==nexttiled(Selmon->Clients))
  if(!c||!(c=nexttiled(c->N)))
   return;
 pop(c);
}

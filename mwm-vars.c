/* variables */
static  const  char broken[]   ="broken";
static         char stext [256]         ;
static          int screen              ,
                    sv,sw               , /* X display screen geometry verth,width */
                    bh,blw     =0       ; /* bar geometry */
static          int (*xerrorxlib)(Display*,XErrorEvent*);
static unsigned int numlockmask=0;
/*
static void (*handler[LASTEvent]) (XEvent *) = {
	[ButtonPress] = buttonpress,
	[ClientMessage] = clientmessage,
	[ConfigureRequest] = configurerequest,
	[ConfigureNotify] = configurenotify,
	[DestroyNotify] = destroynotify,
	[EnterNotify] = enternotify,
	[Expose] = expose,
	[FocusIn] = focusin,
	[KeyPress] = keypress,
	[MappingNotify] = mappingnotify,
	[MapRequest] = maprequest,
	[MotionNotify] = motionnotify,
	[PropertyNotify] = propertynotify,
	[UnmapNotify] = unmapnotify
};
*/
static        Atom    wmatom [WMLast]      ,
                      netatom[NetLast]     ;
static        Bool    running         =True;
static        Cursor  cursor [CurLast]     ;
static        Display*Dpy                  ;
static struct DC      dc                   ;
static struct Monitor*Mons            =NULL,
                     *Selmon          =NULL;
static        Window  root                 ;
